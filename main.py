from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from marketing.spiders.bedandbreakfast import BedandbreakfastSpider

 
process = CrawlerProcess(get_project_settings())
process.crawl(BedandbreakfastSpider)
process.start()