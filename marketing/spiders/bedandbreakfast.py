import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule


class BedandbreakfastSpider(CrawlSpider):
    name = 'bedandbreakfast'
    phpsessid = '5bevf473i151id13fe2aemcppm'
    cflb = '0H28ukDT4VZHGHYxxMbQrGrXMJyVz3a96zZf5YdV3Mv'

    def start_requests(self):
        urls = [
            'https://www.bedandbreakfast.nl/bed-and-breakfast/netherlands/country'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    # We start parsing at the Netherlands search page, at the side-bar a list of dutch provinces is available. Lets continue with that list
    def parse(self, response):
        for province in response.css(".popular_destinations li"):
            province_page = province.css('a::attr(href)').get()
            if province_page is not None:
                province_page = response.urljoin(province_page)
                yield scrapy.Request(province_page, callback=self.parseCityMenu)

    # For every province, we scrape the cities
    def parseCityMenu(self, response):
        for city in response.css(".popular_destinations li"):
            city_page = city.css('a::attr(href)').get()
            if city_page is not None:
                city_page = response.urljoin(city_page)
                yield scrapy.Request(city_page, callback=self.parse_city_page)

    # If we have the cities, we start scraping the bnbs
    def parse_city_page(self, response):
        for bnb in response.css(".bnb-item"):
            bnb_page = bnb.css('a::attr(href)').get()
            if bnb_page is not None:
                bnb_page = response.urljoin(bnb_page)
                yield scrapy.Request(bnb_page, callback=self.parse_bnb_page)

    # Parse the final page
    def parse_bnb_page(self, response):
        text_count = len(response.css('.description-text .description-pretext::text').get())
        extra_text_count = response.css('.description-text .extra::text').get()
        
        if extra_text_count is not None:
            text_count = text_count + len(extra_text_count)

        yield {
            'location': response.css('.area a::text').get(),
            'name': response.css('.title h1::text').get(),
            'average-rating': response.css('.grade .number::text').get(),
            'amount': response.css('.amount::text').get(),
            'summary-tags': response.css('.list li::text').getall(),
            'nr-of-images': len(response.css('.cycle_pager .slide_thumb').getall()),
            'nr-of-videos': len(response.css('.bnb-video').getall()),
            'description-text-count': text_count
        }